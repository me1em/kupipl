# -*- coding: utf-8 -*-

URL = "https://by.e-konsulat.gov.pl"
CHECK_ERROR_TIMEOUT = 3

PAGE_LANG = {'id': 'ddlWersjeJezykowe', 'value': 'Русская'}

DATES = {'id': 'cp_cbDzien'}
REG_BTN = {'id': 'cp_btnRezerwuj'}
ERROR_MSG = {
    u'blad': u'Błąd ogólny',
    u'reject_error': u'The requested URL was rejected'
}

SURNAME_FIELD = ('input', 'cp_f_daneOs_txtNazwisko')
EX_SURNAME_FIELD = ('input', 'cp_f_daneOs_txtNazwiskoRodowe')
NAME_FIELD = ('input', 'cp_f_daneOs_txtImiona')
DATE_OF_BIRTH_FIELD = ('input', 'cp_f_daneOs_txtDataUrodzin')
PLACE_OF_BIRTH_FIELD = ('input', 'cp_f_daneOs_txtMiejsceUrodzenia')
COUNTRY_OF_BIRTH_FIELD = ('select', 'cp_f_daneOs_cbKrajUrodzenia')
CITIZENSHIP_FIELD = ('select', 'cp_f_daneOs_cbObecneObywatelstwo')
BIRTH_CITIZENSHIP_FIELD = ('select', 'cp_f_daneOs_cbPosiadaneObywatelstwo')
SEX_M_FIELD = ('radio input', 'cp_f_daneOs_rbPlec_0')
SEX_F_FIELD = ('radio input', 'cp_f_daneOs_rbPlec_1')
FAMILY_STATE_FIELD = {
    u'Холост/не замужем': 'cp_f_daneOs_rbStanCywilny_0',
    u'Женат/Замужем': 'cp_f_daneOs_rbStanCywilny_1',
    u'не проживает с супругом': 'cp_f_daneOs_rbStanCywilny_2',
    u'Разведен/-а': 'cp_f_daneOs_rbStanCywilny_3',
    u'Вдовец/вдова': 'cp_f_daneOs_rbStanCywilny_4',
    u'другой': 'cp_f_daneOs_rbStanCywilny_5'
}
ID_NUMBER_FIELD = ('input', 'cp_f_txt5NumerDowodu')
DOC_TYPE_FIELD = {
    u'Обычный паспорт': 'cp_f_rbl13_0',
    u'Дипломатический паспорт': 'cp_f_rbl13_1',
    u'Служебный паспорт': 'cp_f_rbl13_2',
    u'Официальный паспорт': 'cp_f_rbl13_3',
    u'Особый паспорт': 'cp_f_rbl13_4',
    u'Иной проездной документ (указать, какой)': 'cp_f_rbl13_5'
}
DOC_NUMBER_FIELD = ('input', 'cp_f_txt14NumerPaszportu')
DOC_START_FIELD = ('input', 'cp_f_txt16WydanyDnia')
DOC_FINISH_FIELD = ('input', 'cp_f_txt17WaznyDo')
DOC_RUVD_FIELD = ('input', 'cp_f_txt15WydanyPrzez')
ADULT_FIELD = {
    u'да': 'cp_f_opiekunowie_chkNieDotyczy'
}

HOME_COUNTRY = ('select', 'cp_f_ddl45Panstwo')
HOME_PROV = ('input', 'cp_f_txt45StanProwincja')
HOME_CITY = ('input', 'cp_f_txt45Miejscowosc')
HOME_ZIP_CODE = ('input', 'cp_f_txt45Kod')
HOME_STREET = ('input', 'cp_f_txt45Adres')
HOME_EMAIL = ('input', 'cp_f_txt17Email')
HOME_PHONE_CODE = ('input', 'cp_f_txt46TelefonPrefiks0')
HOME_PHONE_NUM = ('input', 'cp_f_txt46TelefonNumer0')

GUEST_COUNTRY = {
    u'Нет': 'cp_f_rbl18_0',
    u'Да. Вид на жительство или равноценный документ': 'cp_f_rbl18_1'
}

PROFY_FIELD = ('select', 'cp_f_ddl19WykonywanyZawod')

JOB_OR_STUDY = {
    u'Работодатель': 'cp_f_rbl20_0',
    u'Учебное заведение': 'cp_f_rbl20_1'
}

JOB_COUNTRY = ('select', 'cp_f_dd20bPanstwo')
JOB_PROV = ('input', 'cp_f_txt20cStanProwincja')
JOB_CITY = ('input', 'cp_f_txt20dMiejscowosc')
JOB_ZIP_CODE = ('input', 'cp_f_txt20eKodPocztowy')
JOB_STREET = ('input', 'cp_f_txt20fAdres')
JOB_PHONE_CODE = ('input', 'cp_f_txt20gPrefix')
JOB_PHONE_NUM = ('input', 'cp_f_txt20hTelefon')
JOB_NAME = ('input', 'cp_f_txt20Nazwa')
JOB_EMAIL = ('input', 'cp_f_txt20Email')
JOB_FAX_CODE = ('input', 'cp_f_txt20PrefiksFax')
JOB_FAX_NUM = ('input', 'cp_f_txt20NumerFax')

TARGET_FIELD = ('checkbox', 'cp_f_rbl29_10')
TARGET_DESC_FIELD = ('input', 'cp_f_txt29CelPodrozy')
TARGET_COUNTRY_FIELD = ('input', 'cp_f_ddl21KrajDocelowy')
FIRST_COUNTRY_FIELD = ('input', 'cp_f_ddl23PierwszyWjazd')

VISA_TERM_FIELD = {
    u'Однократного въезда': 'cp_f_rbl24_0',
    u'Двукратного въезда': 'cp_f_rbl24_1',
    u'Многократного въезда': 'cp_f_rbl24_2',
}
VISA_DAYS_FIELD = ('input', 'cp_f_txt25OkresPobytu')
FIRST_COME_FIELD = ('input', 'cp_f_txt30DataWjazdu')
LEAVE_FIELD = ('input', 'cp_f_txt31DataWyjazdu')
EX_VISAS_FIELD = {
    u'Нет': 'cp_f_rbl26_0',
    u'Да. Срок действия (год-месяц-день)': 'cp_f_rbl26_1'
}
EX_VISAS_BTN_ADD_FIELD = ('input', 'cp_f_btn26Wiecej')
EX_VISAS_1_FROM = ('input', 'PoprzednieWizy_0_txtDataOd')
EX_VISAS_1_TO = ('input', 'PoprzednieWizy_0_txtDataDo')
EX_VISAS_2_FROM = ('input', 'PoprzednieWizy_1_txtDataOd')
EX_VISAS_2_TO = ('input', 'PoprzednieWizy_1_txtDataDo')
EX_VISAS_3_FROM = ('input', 'PoprzednieWizy_2_txtDataOd')
EX_VISAS_3_TO = ('input', 'PoprzednieWizy_2_txtDataDo')

FINGERPRINTS_FIELD = {
    u'Нет': 'cp_f_rbl27_0',
    u'Да': 'cp_f_rbl27_1'
}
RESOLUTION_FIELD = ('checkbox', 'cp_f_chkNiedotyczy28')

INVITATION_TYPE_FIELD = {
    u'человек': 'cp_f_ctrl31__rbl34_0',
    u'фирма': 'cp_f_ctrl31__rbl34_1'
}

INV_NAME = ('input', 'cp_f_ctrl31__txt34Nazwa')
INV_NAME1 = ('input', 'cp_f_ctrl31__txt34Imie')
INV_SURNAME = ('input', 'cp_f_ctrl31__txt34Nazwisko')
INV_COUNTRY = ('select', 'cp_f_ctrl31__ddl34panstwo')
INV_CITY = ('input', 'cp_f_ctrl31__txt34miejscowosc')
INV_ZIP = ('input', 'cp_f_ctrl31__txt34kod')
INV_TEL_PREF = ('input', 'cp_f_ctrl31__txt34prefikstel')
INV_TEL_NUM = ('input', 'cp_f_ctrl31__txt34tel')
INV_FAX_PREF = ('input', 'cp_f_ctrl31__txt34prefiksfax')
INV_FAX_NUM = ('input', 'cp_f_ctrl31__txt34fax')
INV_ADDR = ('input', 'cp_f_ctrl31__txt34adres')
INV_BUILDING = ('input', 'cp_f_ctrl31__txt34NumerDomu')
INV_FLAT = ('input', 'cp_f_ctrl31__txt34NumerLokalu')
INV_EMAIL = ('input', 'cp_f_ctrl31__txt34Email')

OWN_MONEY = ('input', 'cp_f_rbl35_0')
MONEY_CASH = ('input', 'cp_f_rb36Gotowka')

NOT_EU_CITIZEN = ('input', 'cp_f_chkNieDotyczy43')

ACCEPTED_CHECKS = (
    'cp_f_chk44Oswiadczenie1',
    'cp_f_chk44Oswiadczenie2',
    'cp_f_chk44Oswiadczenie3'
)

BTN_NEXT_DEBUG = ('input', 'cp_f_cmdDalej')
CAPTCHA_FIELD = ('input', 'cp_BotDetectCaptchaCodeTextBox')
DATE_OFF_FIELD = ('label', 'cp_lblBrakTerminow')