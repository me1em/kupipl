# -*- coding: utf-8 -*-
from settings import *

TIME_FOR_CAPTCHA_IN_SEC = 30

LOCAL_URL = "https://by.e-konsulat.gov.pl/Informacyjne/Placowka.aspx?IDPlacowki=93"

CITY = {'id': 'tresc_cbListaPlacowek', 'value': 'Брест'}
REG_LINK = 'Шенгенская Виза - Зарегистрируйте бланк'
VISA_TYPE = {'id': 'cp_cbRodzajUslugi', 'value': 'ШЕНГЕН – ВОДИТЕЛИ (TIR)'}
