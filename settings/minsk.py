# -*- coding: utf-8 -*-
from settings import *


TIME_FOR_CAPTCHA_IN_SEC = 20

URL = "https://by.e-konsulat.gov.pl"

LOCAL_URL = "https://by.e-konsulat.gov.pl/Informacyjne/Placowka.aspx?IDPlacowki=94"

PAGE_LANG = {'id': 'ddlWersjeJezykowe', 'value': 'Русская'}
CITY = {'id': 'tresc_cbListaPlacowek', 'value': 'Минск'}
REG_LINK = 'Шенгенская Виза - Зарегистрируйте бланк'
VISA_TYPE = {'id': 'cp_cbRodzajUslugi', 'value': 'ВИЗЫ С ИНОЙ ЦЕЛЬЮ'}