# -*- coding: utf-8 -*-
from __future__ import print_function
import argparse

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException

import time
import user_parser


class VisaFlow():
    def __init__(self, file_name):
        self.file_name = file_name
        self.browser = webdriver.Firefox()
        # self.browser.maximize_window()
        self.browser.implicitly_wait(6)

    def _check_for_errors(self):
        print("check for errors...")

        time.sleep(settings.CHECK_ERROR_TIMEOUT)

        '''
        try:
            body = WebDriverWait(self.browser, 30).until(
                EC.presence_of_element_located((By.TAG_NAME, 'body'))
            )
        except NoSuchElementException:
            print("body error detection")
        '''

        body = self.browser.find_element_by_tag_name('body')

        if settings.ERROR_MSG[u'blad'] in body.text:
            print("blad detect")
            self.browser.back()
            try:
                self.browser.switch_to.alert()
            except Exception:
                pass
            self._check_for_errors()
        elif settings.ERROR_MSG[u'reject_error'] in body.text:
            print("reject detect")
            # self.browser.back()
            self.browser.refresh()
            self.browser.switch_to.alert.accept()
            self._check_for_errors()
        else:
            print("check good")

    def _fill_form(self, user):
        print("filling form")
        # surname
        surname_input = self.browser.find_element_by_id(settings.SURNAME_FIELD[1])
        surname_input.send_keys(user[0][1])

        # ex_surname
        surname_input = self.browser.find_element_by_id(settings.EX_SURNAME_FIELD[1])
        surname_input.send_keys(user[1][1])

        # name
        name_input = self.browser.find_element_by_id(settings.NAME_FIELD[1])
        name_input.send_keys(user[2][1])

        # date of birth
        date_of_birth_input = self.browser.find_element_by_id(settings.DATE_OF_BIRTH_FIELD[1])
        date_of_birth_input.send_keys(user[3][1])

        # place of birth
        place_of_birth_input = self.browser.find_element_by_id(settings.PLACE_OF_BIRTH_FIELD[1])
        place_of_birth_input.send_keys(user[4][1])

        # country of birth
        country_select = Select(self.browser.find_element_by_id(settings.COUNTRY_OF_BIRTH_FIELD[1]))
        country_select.select_by_visible_text(user[5][1])

        # citizenship
        citizenship_select = Select(self.browser.find_element_by_id(settings.CITIZENSHIP_FIELD[1]))
        citizenship_select.select_by_visible_text(user[6][1])

        # b_citizenship
        b_citizenship_select = Select(self.browser.find_element_by_id(settings.BIRTH_CITIZENSHIP_FIELD[1]))
        b_citizenship_select.select_by_visible_text(user[7][1])

        # sex
        if user[8][1].lower() in [u'Муж']:
            self.browser.find_element_by_id(settings.SEX_M_FIELD[1]).click()
        else:
            self.browser.find_element_by_id(settings.SEX_F_FIELD[1]).click()

        # family state
        self.browser.find_element_by_id(settings.FAMILY_STATE_FIELD[user[9][1]]).click()

        # id
        id_input = self.browser.find_element_by_id(settings.ID_NUMBER_FIELD[1])
        id_input.send_keys(user[10][1])

        # doc type
        self.browser.find_element_by_id(settings.DOC_TYPE_FIELD[user[11][1]]).click()

        # doc number
        self.browser.find_element_by_id(settings.DOC_NUMBER_FIELD[1]).send_keys(user[13][1])
        # start date
        self.browser.find_element_by_id(settings.DOC_START_FIELD[1]).send_keys(user[14][1])
        # finish date
        self.browser.find_element_by_id(settings.DOC_FINISH_FIELD[1]).send_keys(user[15][1])
        # RUVD
        self.browser.find_element_by_id(settings.DOC_RUVD_FIELD[1]).send_keys(user[16][1])

        # are you 18teen?
        self.browser.find_element_by_id(settings.ADULT_FIELD[user[17][1].lower()]).click()

        # home address
        # country
        Select(self.browser.find_element_by_id(
            settings.HOME_COUNTRY[1]
        )).select_by_visible_text(user[18][1])

        self.browser.find_element_by_id(settings.HOME_PROV[1]).send_keys(user[19][1])
        self.browser.find_element_by_id(settings.HOME_CITY[1]).send_keys(user[20][1])
        self.browser.find_element_by_id(settings.HOME_ZIP_CODE[1]).send_keys(user[21][1])
        self.browser.find_element_by_id(settings.HOME_STREET[1]).send_keys(user[22][1])
        self.browser.find_element_by_id(settings.HOME_EMAIL[1]).send_keys(user[23][1])
        self.browser.find_element_by_id(settings.HOME_PHONE_CODE[1]).send_keys(user[24][1])
        self.browser.find_element_by_id(settings.HOME_PHONE_NUM[1]).send_keys(user[25][1])

        # guest country
        self.browser.find_element_by_id(settings.GUEST_COUNTRY[user[26][1]]).click()

        # vid na zhitelstvo
        # nomer - user[27][1]
        # srok - user[28][1] or bessrochno

        # profy
        Select(self.browser.find_element_by_id(
            settings.PROFY_FIELD[1]
        )).select_by_visible_text(user[29][1])

        # job or study
        self.browser.find_element_by_id(settings.JOB_OR_STUDY[user[30][1]]).click()

        Select(self.browser.find_element_by_id(
            settings.JOB_COUNTRY[1]
        )).select_by_visible_text(user[31][1])

        self.browser.find_element_by_id(settings.JOB_PROV[1]).send_keys(user[32][1])
        self.browser.find_element_by_id(settings.JOB_CITY[1]).send_keys(user[33][1])
        self.browser.find_element_by_id(settings.JOB_ZIP_CODE[1]).send_keys(user[34][1])
        self.browser.find_element_by_id(settings.JOB_STREET[1]).send_keys(user[35][1])
        self.browser.find_element_by_id(settings.JOB_PHONE_CODE[1]).send_keys(user[36][1])
        self.browser.find_element_by_id(settings.JOB_PHONE_NUM[1]).send_keys(user[37][1])
        self.browser.find_element_by_id(settings.JOB_NAME[1]).send_keys(user[38][1])
        self.browser.find_element_by_id(settings.JOB_EMAIL[1]).send_keys(user[39][1])
        self.browser.find_element_by_id(settings.JOB_FAX_CODE[1]).send_keys(user[40][1])
        self.browser.find_element_by_id(settings.JOB_FAX_NUM[1]).send_keys(user[41][1])

        # target
        self.browser.find_element_by_id(settings.TARGET_FIELD[1]).click()
        # target desc
        self.browser.find_element_by_id(settings.TARGET_DESC_FIELD[1]).send_keys(user[42][1])

        # target country
        Select(self.browser.find_element_by_id(
            settings.TARGET_COUNTRY_FIELD[1]
        )).select_by_visible_text(user[43][1])

        # first country
        Select(self.browser.find_element_by_id(
            settings.FIRST_COUNTRY_FIELD[1]
        )).select_by_visible_text(user[44][1])

        # visa term
        self.browser.find_element_by_id(settings.VISA_TERM_FIELD[user[45][1]]).click()
        # visa days
        self.browser.find_element_by_id(settings.VISA_DAYS_FIELD[1]).send_keys(user[46][1])

        # first come
        self.browser.find_element_by_id(settings.FIRST_COME_FIELD[1]).send_keys(user[47][1])
        # leave
        self.browser.find_element_by_id(settings.LEAVE_FIELD[1]).send_keys(user[48][1])

        # ex visas
        self.browser.find_element_by_id(settings.EX_VISAS_FIELD[user[49][1]]).click()

        # ex visas button 2 click
        ex_visas_btn = self.browser.find_element_by_id(settings.EX_VISAS_BTN_ADD_FIELD[1])

        # ex visa 1
        if self.browser.find_element_by_id(settings.EX_VISAS_1_FROM[1]).is_enabled():
            self.browser.find_element_by_id(settings.EX_VISAS_1_FROM[1]).send_keys(user[50][1])
            self.browser.find_element_by_id(settings.EX_VISAS_1_TO[1]).send_keys(user[51][1])
        # ex visa 2
        if user[52][1] != '':
            ex_visas_btn.click()
            self.browser.find_element_by_id(settings.EX_VISAS_2_FROM[1]).send_keys(user[52][1])
            self.browser.find_element_by_id(settings.EX_VISAS_2_TO[1]).send_keys(user[53][1])
        # ex visa 3
        if user[54][1] != '':
            ex_visas_btn.click()
            self.browser.find_element_by_id(settings.EX_VISAS_3_FROM[1]).send_keys(user[54][1])
            self.browser.find_element_by_id(settings.EX_VISAS_3_TO[1]).send_keys(user[55][1])

        # fingerprint
        self.browser.find_element_by_id(settings.FINGERPRINTS_FIELD[user[56][1]]).click()

        # resolution checkbox click
        self.browser.find_element_by_id(settings.RESOLUTION_FIELD[1]).click()

        # invitation type
        self.browser.find_element_by_id(settings.INVITATION_TYPE_FIELD[user[57][1]]).click()

        # invitator data
        if self.browser.find_element_by_id(settings.INV_NAME[1]).is_enabled():
            self.browser.find_element_by_id(settings.INV_NAME[1]).send_keys(user[58][1])
        if self.browser.find_element_by_id(settings.INV_NAME1[1]).is_enabled():
            self.browser.find_element_by_id(settings.INV_NAME1[1]).send_keys(user[59][1])
        if self.browser.find_element_by_id(settings.INV_SURNAME[1]).is_enabled():
            self.browser.find_element_by_id(settings.INV_SURNAME[1]).send_keys(user[60][1])

        Select(self.browser.find_element_by_id(
            settings.INV_COUNTRY[1]
        )).select_by_visible_text(user[61][1])

        self.browser.find_element_by_id(settings.INV_CITY[1]).send_keys(user[62][1])
        self.browser.find_element_by_id(settings.INV_ZIP[1]).send_keys(user[63][1])
        self.browser.find_element_by_id(settings.INV_TEL_PREF[1]).send_keys(user[64][1])
        self.browser.find_element_by_id(settings.INV_TEL_NUM[1]).send_keys(user[65][1])
        self.browser.find_element_by_id(settings.INV_FAX_PREF[1]).send_keys(user[66][1])
        self.browser.find_element_by_id(settings.INV_FAX_NUM[1]).send_keys(user[67][1])
        self.browser.find_element_by_id(settings.INV_ADDR[1]).send_keys(user[68][1])
        self.browser.find_element_by_id(settings.INV_BUILDING[1]).send_keys(user[69][1])
        self.browser.find_element_by_id(settings.INV_FLAT[1]).send_keys(user[70][1])
        self.browser.find_element_by_id(settings.INV_EMAIL[1]).send_keys(user[71][1])

        # own money
        self.browser.find_element_by_id(settings.OWN_MONEY[1]).click()
        # money cash
        self.browser.find_element_by_id(settings.MONEY_CASH[1]).click()

        # not eu citizen
        self.browser.find_element_by_id(settings.NOT_EU_CITIZEN[1]).click()

        # ACCEPTED_CHECKS
        self.browser.find_element_by_id(settings.ACCEPTED_CHECKS[0]).click()
        self.browser.find_element_by_id(settings.ACCEPTED_CHECKS[1]).click()
        self.browser.find_element_by_id(settings.ACCEPTED_CHECKS[2]).click()

        # click btn
        # DEBUG
        # self.browser.find_element_by_id(settings.BTN_NEXT_DEBUG[1]).click()
        self.browser.execute_script('alert("Анкета заполнена. Нажмите кнопку внизу анкеты для окончания регистрации.")')

    def reg(self):

        user_data = user_parser.get_user_data_list(self.file_name)
        # pprint.pprint(user_data)
        self._fill_form(user_data)

    def flow_before_reg(self):
        print("before reg flow")
        print("get konsulat url")
        self.browser.get(settings.LOCAL_URL)
        self._check_for_errors()

        print("try to click reg link")
        try:
            element = WebDriverWait(self.browser, 30).until(
                EC.presence_of_element_located((By.LINK_TEXT, settings.REG_LINK))
            )
            element.click()
        except NoSuchElementException:
            self._check_for_errors()

        while True:
            # captha for user
            self.browser.find_element_by_id(settings.CAPTCHA_FIELD[1])
            print("sleep while captcha processing")
            self.browser.execute_script('alert("Введите капчу. Нажмите кнопку Далее")')
            time.sleep(settings.TIME_FOR_CAPTCHA_IN_SEC)
            self._check_for_errors()

            # captcha or visa type?
            try:
                if self.browser.find_element_by_id(settings.CAPTCHA_FIELD[1]):
                    continue
            except NoSuchElementException:
                try:
                    print("check visa type")
                    visa_type_select = Select(self.browser.find_element_by_id(settings.VISA_TYPE['id']))
                    visa_type_select.select_by_visible_text(settings.VISA_TYPE['value'])
                except NoSuchElementException:
                    pass
            self._check_for_errors()

            # captcha or dates or dates off
            try:
                if self.browser.find_element_by_id(settings.CAPTCHA_FIELD[1]):
                    continue
            except NoSuchElementException:
                try:
                    print("try to select date")
                    dates_select = Select(self.browser.find_element_by_id(settings.DATES['id']))
                    dates_select.select_by_index(1)  # select first date in select box
                    break
                except NoSuchElementException:
                    print("dates not found")
                    try:
                        self.browser.find_element_by_id(settings.DATE_OFF_FIELD[1])
                        self.browser.refresh()
                        self.browser.switch_to.alert.accept()
                    except NoSuchElementException:
                        pass
                self._check_for_errors()

        self._check_for_errors()

        print("click reg button")
        self.browser.find_element_by_id(settings.REG_BTN['id']).click()
        self._check_for_errors()

    def flow_before_reg_debug(self):
        """ test flow before filling form """
        print("debug: before reg flow")
        print("debug: get konsulat url")
        self.browser.get(settings.LOCAL_URL)
        self._check_for_errors()

        print("debug: try to click test reg link")
        '''
        try:
            element = WebDriverWait(self.browser, 10).until(
                EC.presence_of_element_located((By.LINK_TEXT, u"Шенгенская Виза - Заполните бланк"))
            )
            element.click()
        finally:
            self.browser.quit()
        '''
        try:
            self.browser.find_element_by_link_text(u'Шенгенская Виза - Заполните бланк').click()
        except NoSuchElementException:
            pass

        self._check_for_errors()

    def tear_down(self):
        self.browser.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--city', default='minsk')
    parser.add_argument('--debug', action="store_true")
    parser.add_argument('--file', default='user')
    args = parser.parse_args()

    if args.city == 'minsk':
        import settings.minsk as settings
    elif args.city == 'brest':
        import settings.brest as settings
    print("city: " + args.city)
    print("user: " + args.file)

    visa_flow = VisaFlow(file_name=args.file)
    if args.debug:
        visa_flow.flow_before_reg_debug()
    else:
        visa_flow.flow_before_reg()

    visa_flow.reg()




