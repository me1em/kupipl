# -*- coding: utf-8 -*-
from __future__ import print_function
import xlrd
import os
from settings import settings

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


def get_user_data_list(file_name):
    rb = xlrd.open_workbook(BASE_DIR + '/kupipl/user_data/' + file_name, formatting_info=True)
    sheet = rb.sheet_by_index(0)
    user_data = []
    for row_num in range(sheet.nrows):
        row = sheet.row_values(row_num, 0, 2)
        field = row[0].strip()
        data = row[1].strip()
        user_data.append((field, data))

    return user_data
